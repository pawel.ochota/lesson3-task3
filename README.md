# Lekcja 3
## Zadanie 3 - basic 
Napisz funkcję `iterateAndSum`, która za pomocą pętli `while` zsumuje wszystkie elementy przekazanej jako argument tablicy i zwróci tą sumę.
Zadanie także do sprawdzenia manualnego.

Przykładowy input:
`iterateAndSum([1, 2, 3, 4]);`

Przykładowy output:
`10`
